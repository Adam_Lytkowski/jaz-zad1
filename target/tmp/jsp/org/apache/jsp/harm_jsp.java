package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class harm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=ISO-8859-1");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\r\n");
      out.write("<title>Formularz kredytu</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\t");

	java.text.DecimalFormat df = new java.text.DecimalFormat();
	df.setMaximumFractionDigits(2);
	df.setMinimumFractionDigits(2);
	
	String kwota = request.getParameter("kwota");
	String rata = request.getParameter("rata");
	String procent = request.getParameter("procent");
	String oplata = request.getParameter("oplata");
	String rodzaj = request.getParameter("rodzaj");
		response.setContentType("text/html");
		double kwotaNum = Double.parseDouble(kwota);
		double rataNum = Double.parseDouble(rata);
		double procentNum = (Double.parseDouble(procent)/100);
		double oplataNum = Double.parseDouble(oplata);
		double aktualnaKwota = kwotaNum;
		double kwotaKapitalu= kwotaNum/rataNum,kwotaOdsetek, calkowitaRata,q,rataStala;

		
		if(kwotaNum<9999 || kwotaNum>1000000)
		{
		response.sendRedirect("/error");	
		}	
		
		if(rataNum<12 || rataNum>400)
		{
		response.sendRedirect("/error");	
		}	
		
		
	if(rodzaj.equals("malejaca")){
	
      out.write("\r\n");
      out.write("\t<table border=\"1\">\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t<th>Nr raty</th>\r\n");
      out.write("\t<th>Kwota Kapitalu</th>\r\n");
      out.write("\t<th>Kwota odsetek</th>\r\n");
      out.write("\t<th>Oplaty stale</th>\r\n");
      out.write("\t<th>Calkowita kwota raty</th>\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t");
 		
	kwotaKapitalu*=100;
	kwotaKapitalu = Math.round(kwotaKapitalu);
	kwotaKapitalu /= 100;
	
	
	for(int i=1;i<=rataNum;i++){
	kwotaOdsetek = (aktualnaKwota * procentNum)/12;
	calkowitaRata = kwotaKapitalu + kwotaOdsetek + oplataNum;
	
	kwotaOdsetek*=100;
	kwotaOdsetek = Math.round(kwotaOdsetek);
	kwotaOdsetek /= 100;
	
	calkowitaRata*=100;
	calkowitaRata = Math.round(calkowitaRata);
	calkowitaRata /= 100;
	
      out.write("\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t<td> ");
      out.print( i );
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( kwotaKapitalu );
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( kwotaOdsetek );
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( oplataNum);
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( calkowitaRata  );
      out.write("</td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t");

	aktualnaKwota -= kwotaKapitalu;
	}}
	
	else{
	q = 1+(procentNum/12);	
	rataStala = kwotaNum * (Math.pow(q,rataNum))*((q-1)/(Math.pow(q,rataNum)-1));
	
	rataStala*=100;
	rataStala = Math.round(rataStala);
	rataStala /= 100;
	
      out.write("\r\n");
      out.write("\t<table border=\"1\">\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t<th>Stala rata</th>\r\n");
      out.write("\t<th>Oplaty stale(wliczone)</th>\r\n");
      out.write("\t<th>Oprocentowanie</th>\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t<tr>\r\n");
      out.write("\t<td>");
      out.print( rataStala );
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( oplataNum );
      out.write("</td>\r\n");
      out.write("\t<td>");
      out.print( procentNum );
      out.write("</td>\r\n");
      out.write("\t</tr>\r\n");
      out.write("\t");

	}
	
      out.write("\r\n");
      out.write("\t\r\n");
      out.write("\t\r\n");
      out.write("\t</table>\r\n");
      out.write("\t\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
