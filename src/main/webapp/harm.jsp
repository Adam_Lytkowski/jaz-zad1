<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Formularz kredytu</title>
</head>
<body>



	<%
	java.text.DecimalFormat df = new java.text.DecimalFormat();
	df.setMaximumFractionDigits(2);
	df.setMinimumFractionDigits(2);
	
	String kwota = request.getParameter("kwota");
	String rata = request.getParameter("rata");
	String procent = request.getParameter("procent");
	String oplata = request.getParameter("oplata");
	String rodzaj = request.getParameter("rodzaj");
		response.setContentType("text/html");
		double kwotaNum = Double.parseDouble(kwota);
		double rataNum = Double.parseDouble(rata);
		double procentNum = (Double.parseDouble(procent)/100);
		double oplataNum = Double.parseDouble(oplata);
		double aktualnaKwota = kwotaNum;
		double kwotaKapitalu= kwotaNum/rataNum,kwotaOdsetek, calkowitaRata,q,rataStala;

		
		if(kwotaNum<9999 || kwotaNum>1000000)
		{
		response.sendRedirect("/error");	
		}	
		
		if(rataNum<12 || rataNum>400)
		{
		response.sendRedirect("/error");	
		}	
		
		
	if(rodzaj.equals("malejaca")){
	%>
	<table border="1">
	<tr>
	<th>Nr raty</th>
	<th>Kwota Kapitalu</th>
	<th>Kwota odsetek</th>
	<th>Oplaty stale</th>
	<th>Calkowita kwota raty</th>
	</tr>
	<% 		
	kwotaKapitalu*=100;
	kwotaKapitalu = Math.round(kwotaKapitalu);
	kwotaKapitalu /= 100;
	
	
	for(int i=1;i<=rataNum;i++){
	kwotaOdsetek = (aktualnaKwota * procentNum)/12;
	calkowitaRata = kwotaKapitalu + kwotaOdsetek + oplataNum;
	
	kwotaOdsetek*=100;
	kwotaOdsetek = Math.round(kwotaOdsetek);
	kwotaOdsetek /= 100;
	
	calkowitaRata*=100;
	calkowitaRata = Math.round(calkowitaRata);
	calkowitaRata /= 100;
	%>
	<tr>
	<td> <%= i %></td>
	<td><%= kwotaKapitalu %></td>
	<td><%= kwotaOdsetek %></td>
	<td><%= oplataNum%></td>
	<td><%= calkowitaRata  %></td>
	</tr>
	<%
	aktualnaKwota -= kwotaKapitalu;
	}}
	
	else{
	q = 1+(procentNum/12);	
	rataStala = kwotaNum * (Math.pow(q,rataNum))*((q-1)/(Math.pow(q,rataNum)-1));
	
	rataStala*=100;
	rataStala = Math.round(rataStala);
	rataStala /= 100;
	%>
	<table border="1">
	<tr>
	<th>Stala rata</th>
	<th>Oplaty stale(wliczone)</th>
	<th>Oprocentowanie</th>
	
	
	</tr>
	<tr>
	<td><%= rataStala %></td>
	<td><%= oplataNum %></td>
	<td><%= procentNum %></td>
	</tr>
	<%
	}
	%>
	
	
	</table>
	
</body>
</html>